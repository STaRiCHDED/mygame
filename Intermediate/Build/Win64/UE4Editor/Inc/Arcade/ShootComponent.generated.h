// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ARCADE_ShootComponent_generated_h
#error "ShootComponent.generated.h already included, missing '#pragma once' in ShootComponent.h"
#endif
#define ARCADE_ShootComponent_generated_h

#define Arcade_Source_Arcade_Components_ShootComponent_h_13_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FShootInfo_Statics; \
	ARCADE_API static class UScriptStruct* StaticStruct();


template<> ARCADE_API UScriptStruct* StaticStruct<struct FShootInfo>();

#define Arcade_Source_Arcade_Components_ShootComponent_h_32_SPARSE_DATA
#define Arcade_Source_Arcade_Components_ShootComponent_h_32_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execStopShooting); \
	DECLARE_FUNCTION(execStartShooting);


#define Arcade_Source_Arcade_Components_ShootComponent_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execStopShooting); \
	DECLARE_FUNCTION(execStartShooting);


#define Arcade_Source_Arcade_Components_ShootComponent_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUShootComponent(); \
	friend struct Z_Construct_UClass_UShootComponent_Statics; \
public: \
	DECLARE_CLASS(UShootComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcade"), NO_API) \
	DECLARE_SERIALIZER(UShootComponent)


#define Arcade_Source_Arcade_Components_ShootComponent_h_32_INCLASS \
private: \
	static void StaticRegisterNativesUShootComponent(); \
	friend struct Z_Construct_UClass_UShootComponent_Statics; \
public: \
	DECLARE_CLASS(UShootComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcade"), NO_API) \
	DECLARE_SERIALIZER(UShootComponent)


#define Arcade_Source_Arcade_Components_ShootComponent_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShootComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShootComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShootComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShootComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShootComponent(UShootComponent&&); \
	NO_API UShootComponent(const UShootComponent&); \
public:


#define Arcade_Source_Arcade_Components_ShootComponent_h_32_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShootComponent(UShootComponent&&); \
	NO_API UShootComponent(const UShootComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShootComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShootComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UShootComponent)


#define Arcade_Source_Arcade_Components_ShootComponent_h_32_PRIVATE_PROPERTY_OFFSET
#define Arcade_Source_Arcade_Components_ShootComponent_h_29_PROLOG
#define Arcade_Source_Arcade_Components_ShootComponent_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcade_Source_Arcade_Components_ShootComponent_h_32_PRIVATE_PROPERTY_OFFSET \
	Arcade_Source_Arcade_Components_ShootComponent_h_32_SPARSE_DATA \
	Arcade_Source_Arcade_Components_ShootComponent_h_32_RPC_WRAPPERS \
	Arcade_Source_Arcade_Components_ShootComponent_h_32_INCLASS \
	Arcade_Source_Arcade_Components_ShootComponent_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Arcade_Source_Arcade_Components_ShootComponent_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcade_Source_Arcade_Components_ShootComponent_h_32_PRIVATE_PROPERTY_OFFSET \
	Arcade_Source_Arcade_Components_ShootComponent_h_32_SPARSE_DATA \
	Arcade_Source_Arcade_Components_ShootComponent_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	Arcade_Source_Arcade_Components_ShootComponent_h_32_INCLASS_NO_PURE_DECLS \
	Arcade_Source_Arcade_Components_ShootComponent_h_32_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARCADE_API UClass* StaticClass<class UShootComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Arcade_Source_Arcade_Components_ShootComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
