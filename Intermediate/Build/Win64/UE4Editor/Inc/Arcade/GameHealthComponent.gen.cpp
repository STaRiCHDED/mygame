// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Arcade/Components/GameHealthComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGameHealthComponent() {}
// Cross Module References
	ARCADE_API UFunction* Z_Construct_UDelegateFunction_Arcade_HealthsEndedEvent__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_Arcade();
	ARCADE_API UClass* Z_Construct_UClass_UGameHealthComponent_NoRegister();
	ARCADE_API UClass* Z_Construct_UClass_UGameHealthComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_Arcade_HealthsEndedEvent__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Arcade_HealthsEndedEvent__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Components/GameHealthComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_Arcade_HealthsEndedEvent__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_Arcade, nullptr, "HealthsEndedEvent__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_Arcade_HealthsEndedEvent__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Arcade_HealthsEndedEvent__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_Arcade_HealthsEndedEvent__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_Arcade_HealthsEndedEvent__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UGameHealthComponent::execGetHealths)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetHealths();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGameHealthComponent::execChangeHealths)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_ByValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ChangeHealths(Z_Param_ByValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGameHealthComponent::execOnPlayerDamaged)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnPlayerDamaged();
		P_NATIVE_END;
	}
	void UGameHealthComponent::StaticRegisterNativesUGameHealthComponent()
	{
		UClass* Class = UGameHealthComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ChangeHealths", &UGameHealthComponent::execChangeHealths },
			{ "GetHealths", &UGameHealthComponent::execGetHealths },
			{ "OnPlayerDamaged", &UGameHealthComponent::execOnPlayerDamaged },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UGameHealthComponent_ChangeHealths_Statics
	{
		struct GameHealthComponent_eventChangeHealths_Parms
		{
			int32 ByValue;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ByValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGameHealthComponent_ChangeHealths_Statics::NewProp_ByValue = { "ByValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GameHealthComponent_eventChangeHealths_Parms, ByValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGameHealthComponent_ChangeHealths_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGameHealthComponent_ChangeHealths_Statics::NewProp_ByValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGameHealthComponent_ChangeHealths_Statics::Function_MetaDataParams[] = {
		{ "Category", "Game Health" },
		{ "ModuleRelativePath", "Components/GameHealthComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGameHealthComponent_ChangeHealths_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGameHealthComponent, nullptr, "ChangeHealths", nullptr, nullptr, sizeof(GameHealthComponent_eventChangeHealths_Parms), Z_Construct_UFunction_UGameHealthComponent_ChangeHealths_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGameHealthComponent_ChangeHealths_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGameHealthComponent_ChangeHealths_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGameHealthComponent_ChangeHealths_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGameHealthComponent_ChangeHealths()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGameHealthComponent_ChangeHealths_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGameHealthComponent_GetHealths_Statics
	{
		struct GameHealthComponent_eventGetHealths_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGameHealthComponent_GetHealths_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GameHealthComponent_eventGetHealths_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGameHealthComponent_GetHealths_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGameHealthComponent_GetHealths_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGameHealthComponent_GetHealths_Statics::Function_MetaDataParams[] = {
		{ "Category", "Game Health" },
		{ "ModuleRelativePath", "Components/GameHealthComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGameHealthComponent_GetHealths_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGameHealthComponent, nullptr, "GetHealths", nullptr, nullptr, sizeof(GameHealthComponent_eventGetHealths_Parms), Z_Construct_UFunction_UGameHealthComponent_GetHealths_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGameHealthComponent_GetHealths_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGameHealthComponent_GetHealths_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGameHealthComponent_GetHealths_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGameHealthComponent_GetHealths()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGameHealthComponent_GetHealths_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGameHealthComponent_OnPlayerDamaged_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGameHealthComponent_OnPlayerDamaged_Statics::Function_MetaDataParams[] = {
		{ "Comment", "//UFUNCTION()\n//\x09void OnPlayerDamaged(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* EventInstigator, AActor* DamageCauser);\n" },
		{ "ModuleRelativePath", "Components/GameHealthComponent.h" },
		{ "ToolTip", "UFUNCTION()\n       void OnPlayerDamaged(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* EventInstigator, AActor* DamageCauser);" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGameHealthComponent_OnPlayerDamaged_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGameHealthComponent, nullptr, "OnPlayerDamaged", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGameHealthComponent_OnPlayerDamaged_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGameHealthComponent_OnPlayerDamaged_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGameHealthComponent_OnPlayerDamaged()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGameHealthComponent_OnPlayerDamaged_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UGameHealthComponent_NoRegister()
	{
		return UGameHealthComponent::StaticClass();
	}
	struct Z_Construct_UClass_UGameHealthComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HealthsEnded_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_HealthsEnded;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Healths_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Healths;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGameHealthComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_Arcade,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UGameHealthComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UGameHealthComponent_ChangeHealths, "ChangeHealths" }, // 3780608815
		{ &Z_Construct_UFunction_UGameHealthComponent_GetHealths, "GetHealths" }, // 3825263432
		{ &Z_Construct_UFunction_UGameHealthComponent_OnPlayerDamaged, "OnPlayerDamaged" }, // 876879847
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameHealthComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "Comment", "//DECLARE_DYNAMIC_MULTICAST_DELEGATE(FHealthsChangedEvent);\n" },
		{ "IncludePath", "Components/GameHealthComponent.h" },
		{ "ModuleRelativePath", "Components/GameHealthComponent.h" },
		{ "ToolTip", "DECLARE_DYNAMIC_MULTICAST_DELEGATE(FHealthsChangedEvent);" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameHealthComponent_Statics::NewProp_HealthsEnded_MetaData[] = {
		{ "Category", "Game Health" },
		{ "ModuleRelativePath", "Components/GameHealthComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UGameHealthComponent_Statics::NewProp_HealthsEnded = { "HealthsEnded", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGameHealthComponent, HealthsEnded), Z_Construct_UDelegateFunction_Arcade_HealthsEndedEvent__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UGameHealthComponent_Statics::NewProp_HealthsEnded_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGameHealthComponent_Statics::NewProp_HealthsEnded_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameHealthComponent_Statics::NewProp_Healths_MetaData[] = {
		{ "Category", "Game Health" },
		{ "ModuleRelativePath", "Components/GameHealthComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UGameHealthComponent_Statics::NewProp_Healths = { "Healths", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGameHealthComponent, Healths), METADATA_PARAMS(Z_Construct_UClass_UGameHealthComponent_Statics::NewProp_Healths_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGameHealthComponent_Statics::NewProp_Healths_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGameHealthComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameHealthComponent_Statics::NewProp_HealthsEnded,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameHealthComponent_Statics::NewProp_Healths,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGameHealthComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGameHealthComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGameHealthComponent_Statics::ClassParams = {
		&UGameHealthComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UGameHealthComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UGameHealthComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UGameHealthComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGameHealthComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGameHealthComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGameHealthComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGameHealthComponent, 2930635370);
	template<> ARCADE_API UClass* StaticClass<UGameHealthComponent>()
	{
		return UGameHealthComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGameHealthComponent(Z_Construct_UClass_UGameHealthComponent, &UGameHealthComponent::StaticClass, TEXT("/Script/Arcade"), TEXT("UGameHealthComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGameHealthComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
