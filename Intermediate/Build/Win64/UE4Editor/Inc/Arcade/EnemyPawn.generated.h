// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
#ifdef ARCADE_EnemyPawn_generated_h
#error "EnemyPawn.generated.h already included, missing '#pragma once' in EnemyPawn.h"
#endif
#define ARCADE_EnemyPawn_generated_h

#define Arcade_Source_Arcade_Pawns_EnemyPawn_h_32_SPARSE_DATA
#define Arcade_Source_Arcade_Pawns_EnemyPawn_h_32_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execDestroyPawn); \
	DECLARE_FUNCTION(execOnEnemyOverlap); \
	DECLARE_FUNCTION(execKillPawn);


#define Arcade_Source_Arcade_Pawns_EnemyPawn_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execDestroyPawn); \
	DECLARE_FUNCTION(execOnEnemyOverlap); \
	DECLARE_FUNCTION(execKillPawn);


#define Arcade_Source_Arcade_Pawns_EnemyPawn_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEnemyPawn(); \
	friend struct Z_Construct_UClass_AEnemyPawn_Statics; \
public: \
	DECLARE_CLASS(AEnemyPawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcade"), NO_API) \
	DECLARE_SERIALIZER(AEnemyPawn)


#define Arcade_Source_Arcade_Pawns_EnemyPawn_h_32_INCLASS \
private: \
	static void StaticRegisterNativesAEnemyPawn(); \
	friend struct Z_Construct_UClass_AEnemyPawn_Statics; \
public: \
	DECLARE_CLASS(AEnemyPawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcade"), NO_API) \
	DECLARE_SERIALIZER(AEnemyPawn)


#define Arcade_Source_Arcade_Pawns_EnemyPawn_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEnemyPawn(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEnemyPawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemyPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemyPawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemyPawn(AEnemyPawn&&); \
	NO_API AEnemyPawn(const AEnemyPawn&); \
public:


#define Arcade_Source_Arcade_Pawns_EnemyPawn_h_32_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemyPawn(AEnemyPawn&&); \
	NO_API AEnemyPawn(const AEnemyPawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemyPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemyPawn); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AEnemyPawn)


#define Arcade_Source_Arcade_Pawns_EnemyPawn_h_32_PRIVATE_PROPERTY_OFFSET
#define Arcade_Source_Arcade_Pawns_EnemyPawn_h_29_PROLOG
#define Arcade_Source_Arcade_Pawns_EnemyPawn_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcade_Source_Arcade_Pawns_EnemyPawn_h_32_PRIVATE_PROPERTY_OFFSET \
	Arcade_Source_Arcade_Pawns_EnemyPawn_h_32_SPARSE_DATA \
	Arcade_Source_Arcade_Pawns_EnemyPawn_h_32_RPC_WRAPPERS \
	Arcade_Source_Arcade_Pawns_EnemyPawn_h_32_INCLASS \
	Arcade_Source_Arcade_Pawns_EnemyPawn_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Arcade_Source_Arcade_Pawns_EnemyPawn_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcade_Source_Arcade_Pawns_EnemyPawn_h_32_PRIVATE_PROPERTY_OFFSET \
	Arcade_Source_Arcade_Pawns_EnemyPawn_h_32_SPARSE_DATA \
	Arcade_Source_Arcade_Pawns_EnemyPawn_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	Arcade_Source_Arcade_Pawns_EnemyPawn_h_32_INCLASS_NO_PURE_DECLS \
	Arcade_Source_Arcade_Pawns_EnemyPawn_h_32_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARCADE_API UClass* StaticClass<class AEnemyPawn>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Arcade_Source_Arcade_Pawns_EnemyPawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
