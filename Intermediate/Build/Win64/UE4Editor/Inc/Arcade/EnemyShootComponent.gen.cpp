// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Arcade/Components/EnemyShootComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEnemyShootComponent() {}
// Cross Module References
	ARCADE_API UScriptStruct* Z_Construct_UScriptStruct_FEnemyShootInfo();
	UPackage* Z_Construct_UPackage__Script_Arcade();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ARCADE_API UClass* Z_Construct_UClass_AEnemyShootProjectile_NoRegister();
	ARCADE_API UClass* Z_Construct_UClass_UEnemyShootComponent_NoRegister();
	ARCADE_API UClass* Z_Construct_UClass_UEnemyShootComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
// End Cross Module References
class UScriptStruct* FEnemyShootInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ARCADE_API uint32 Get_Z_Construct_UScriptStruct_FEnemyShootInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FEnemyShootInfo, Z_Construct_UPackage__Script_Arcade(), TEXT("EnemyShootInfo"), sizeof(FEnemyShootInfo), Get_Z_Construct_UScriptStruct_FEnemyShootInfo_Hash());
	}
	return Singleton;
}
template<> ARCADE_API UScriptStruct* StaticStruct<FEnemyShootInfo>()
{
	return FEnemyShootInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FEnemyShootInfo(FEnemyShootInfo::StaticStruct, TEXT("/Script/Arcade"), TEXT("EnemyShootInfo"), false, nullptr, nullptr);
static struct FScriptStruct_Arcade_StaticRegisterNativesFEnemyShootInfo
{
	FScriptStruct_Arcade_StaticRegisterNativesFEnemyShootInfo()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("EnemyShootInfo")),new UScriptStruct::TCppStructOps<FEnemyShootInfo>);
	}
} ScriptStruct_Arcade_StaticRegisterNativesFEnemyShootInfo;
	struct Z_Construct_UScriptStruct_FEnemyShootInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnemyAngle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_EnemyAngle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnemyOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EnemyOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnemyProjectileClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_EnemyProjectileClass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnemyShootInfo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "//?????? ???????? ? blueprint\n" },
		{ "ModuleRelativePath", "Components/EnemyShootComponent.h" },
		{ "ToolTip", "?????? ???????? ? blueprint" },
	};
#endif
	void* Z_Construct_UScriptStruct_FEnemyShootInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FEnemyShootInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnemyShootInfo_Statics::NewProp_EnemyAngle_MetaData[] = {
		{ "Category", "Shooting" },
		{ "ModuleRelativePath", "Components/EnemyShootComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FEnemyShootInfo_Statics::NewProp_EnemyAngle = { "EnemyAngle", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEnemyShootInfo, EnemyAngle), METADATA_PARAMS(Z_Construct_UScriptStruct_FEnemyShootInfo_Statics::NewProp_EnemyAngle_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnemyShootInfo_Statics::NewProp_EnemyAngle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnemyShootInfo_Statics::NewProp_EnemyOffset_MetaData[] = {
		{ "Category", "Shooting" },
		{ "ModuleRelativePath", "Components/EnemyShootComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FEnemyShootInfo_Statics::NewProp_EnemyOffset = { "EnemyOffset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEnemyShootInfo, EnemyOffset), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FEnemyShootInfo_Statics::NewProp_EnemyOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnemyShootInfo_Statics::NewProp_EnemyOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnemyShootInfo_Statics::NewProp_EnemyProjectileClass_MetaData[] = {
		{ "Category", "Shooting" },
		{ "ModuleRelativePath", "Components/EnemyShootComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FEnemyShootInfo_Statics::NewProp_EnemyProjectileClass = { "EnemyProjectileClass", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEnemyShootInfo, EnemyProjectileClass), Z_Construct_UClass_AEnemyShootProjectile_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UScriptStruct_FEnemyShootInfo_Statics::NewProp_EnemyProjectileClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnemyShootInfo_Statics::NewProp_EnemyProjectileClass_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FEnemyShootInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnemyShootInfo_Statics::NewProp_EnemyAngle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnemyShootInfo_Statics::NewProp_EnemyOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnemyShootInfo_Statics::NewProp_EnemyProjectileClass,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FEnemyShootInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Arcade,
		nullptr,
		&NewStructOps,
		"EnemyShootInfo",
		sizeof(FEnemyShootInfo),
		alignof(FEnemyShootInfo),
		Z_Construct_UScriptStruct_FEnemyShootInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnemyShootInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FEnemyShootInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnemyShootInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FEnemyShootInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FEnemyShootInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Arcade();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("EnemyShootInfo"), sizeof(FEnemyShootInfo), Get_Z_Construct_UScriptStruct_FEnemyShootInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FEnemyShootInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FEnemyShootInfo_Hash() { return 3568635349U; }
	DEFINE_FUNCTION(UEnemyShootComponent::execStopShooting)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StopShooting();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEnemyShootComponent::execStartShooting)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StartShooting();
		P_NATIVE_END;
	}
	void UEnemyShootComponent::StaticRegisterNativesUEnemyShootComponent()
	{
		UClass* Class = UEnemyShootComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "StartShooting", &UEnemyShootComponent::execStartShooting },
			{ "StopShooting", &UEnemyShootComponent::execStopShooting },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UEnemyShootComponent_StartShooting_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnemyShootComponent_StartShooting_Statics::Function_MetaDataParams[] = {
		{ "Category", "Shooting" },
		{ "Comment", "// Called every frame\n//virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;\n" },
		{ "ModuleRelativePath", "Components/EnemyShootComponent.h" },
		{ "ToolTip", "Called every frame\nvirtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEnemyShootComponent_StartShooting_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEnemyShootComponent, nullptr, "StartShooting", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEnemyShootComponent_StartShooting_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnemyShootComponent_StartShooting_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEnemyShootComponent_StartShooting()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEnemyShootComponent_StartShooting_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEnemyShootComponent_StopShooting_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnemyShootComponent_StopShooting_Statics::Function_MetaDataParams[] = {
		{ "Category", "Shooting" },
		{ "ModuleRelativePath", "Components/EnemyShootComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEnemyShootComponent_StopShooting_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEnemyShootComponent, nullptr, "StopShooting", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEnemyShootComponent_StopShooting_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnemyShootComponent_StopShooting_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEnemyShootComponent_StopShooting()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEnemyShootComponent_StopShooting_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UEnemyShootComponent_NoRegister()
	{
		return UEnemyShootComponent::StaticClass();
	}
	struct Z_Construct_UClass_UEnemyShootComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnemyShootInfos_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_EnemyShootInfos;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EnemyShootInfos_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShootPeriod_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ShootPeriod;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEnemyShootComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_Arcade,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UEnemyShootComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UEnemyShootComponent_StartShooting, "StartShooting" }, // 155970242
		{ &Z_Construct_UFunction_UEnemyShootComponent_StopShooting, "StopShooting" }, // 1177332223
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnemyShootComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "Components/EnemyShootComponent.h" },
		{ "ModuleRelativePath", "Components/EnemyShootComponent.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnemyShootComponent_Statics::NewProp_EnemyShootInfos_MetaData[] = {
		{ "Category", "Shooting" },
		{ "ModuleRelativePath", "Components/EnemyShootComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UEnemyShootComponent_Statics::NewProp_EnemyShootInfos = { "EnemyShootInfos", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEnemyShootComponent, EnemyShootInfos), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UEnemyShootComponent_Statics::NewProp_EnemyShootInfos_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEnemyShootComponent_Statics::NewProp_EnemyShootInfos_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UEnemyShootComponent_Statics::NewProp_EnemyShootInfos_Inner = { "EnemyShootInfos", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FEnemyShootInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnemyShootComponent_Statics::NewProp_ShootPeriod_MetaData[] = {
		{ "Category", "Shooting" },
		{ "ModuleRelativePath", "Components/EnemyShootComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UEnemyShootComponent_Statics::NewProp_ShootPeriod = { "ShootPeriod", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEnemyShootComponent, ShootPeriod), METADATA_PARAMS(Z_Construct_UClass_UEnemyShootComponent_Statics::NewProp_ShootPeriod_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEnemyShootComponent_Statics::NewProp_ShootPeriod_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEnemyShootComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnemyShootComponent_Statics::NewProp_EnemyShootInfos,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnemyShootComponent_Statics::NewProp_EnemyShootInfos_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnemyShootComponent_Statics::NewProp_ShootPeriod,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEnemyShootComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEnemyShootComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEnemyShootComponent_Statics::ClassParams = {
		&UEnemyShootComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UEnemyShootComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UEnemyShootComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UEnemyShootComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEnemyShootComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEnemyShootComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEnemyShootComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEnemyShootComponent, 892997299);
	template<> ARCADE_API UClass* StaticClass<UEnemyShootComponent>()
	{
		return UEnemyShootComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEnemyShootComponent(Z_Construct_UClass_UEnemyShootComponent, &UEnemyShootComponent::StaticClass, TEXT("/Script/Arcade"), TEXT("UEnemyShootComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEnemyShootComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
