// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
#ifdef ARCADE_PlayerPawn_generated_h
#error "PlayerPawn.generated.h already included, missing '#pragma once' in PlayerPawn.h"
#endif
#define ARCADE_PlayerPawn_generated_h

#define Arcade_Source_Arcade_Pawns_PlayerPawn_h_12_DELEGATE \
static inline void FPawnDamagedEvent_DelegateWrapper(const FMulticastScriptDelegate& PawnDamagedEvent) \
{ \
	PawnDamagedEvent.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Arcade_Source_Arcade_Pawns_PlayerPawn_h_18_SPARSE_DATA
#define Arcade_Source_Arcade_Pawns_PlayerPawn_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnPlayerOverlap);


#define Arcade_Source_Arcade_Pawns_PlayerPawn_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnPlayerOverlap);


#define Arcade_Source_Arcade_Pawns_PlayerPawn_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlayerPawn(); \
	friend struct Z_Construct_UClass_APlayerPawn_Statics; \
public: \
	DECLARE_CLASS(APlayerPawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcade"), NO_API) \
	DECLARE_SERIALIZER(APlayerPawn)


#define Arcade_Source_Arcade_Pawns_PlayerPawn_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAPlayerPawn(); \
	friend struct Z_Construct_UClass_APlayerPawn_Statics; \
public: \
	DECLARE_CLASS(APlayerPawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcade"), NO_API) \
	DECLARE_SERIALIZER(APlayerPawn)


#define Arcade_Source_Arcade_Pawns_PlayerPawn_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlayerPawn(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlayerPawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerPawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerPawn(APlayerPawn&&); \
	NO_API APlayerPawn(const APlayerPawn&); \
public:


#define Arcade_Source_Arcade_Pawns_PlayerPawn_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerPawn(APlayerPawn&&); \
	NO_API APlayerPawn(const APlayerPawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerPawn); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlayerPawn)


#define Arcade_Source_Arcade_Pawns_PlayerPawn_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Healths() { return STRUCT_OFFSET(APlayerPawn, Healths); } \
	FORCEINLINE static uint32 __PPO__Score() { return STRUCT_OFFSET(APlayerPawn, Score); }


#define Arcade_Source_Arcade_Pawns_PlayerPawn_h_15_PROLOG
#define Arcade_Source_Arcade_Pawns_PlayerPawn_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcade_Source_Arcade_Pawns_PlayerPawn_h_18_PRIVATE_PROPERTY_OFFSET \
	Arcade_Source_Arcade_Pawns_PlayerPawn_h_18_SPARSE_DATA \
	Arcade_Source_Arcade_Pawns_PlayerPawn_h_18_RPC_WRAPPERS \
	Arcade_Source_Arcade_Pawns_PlayerPawn_h_18_INCLASS \
	Arcade_Source_Arcade_Pawns_PlayerPawn_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Arcade_Source_Arcade_Pawns_PlayerPawn_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcade_Source_Arcade_Pawns_PlayerPawn_h_18_PRIVATE_PROPERTY_OFFSET \
	Arcade_Source_Arcade_Pawns_PlayerPawn_h_18_SPARSE_DATA \
	Arcade_Source_Arcade_Pawns_PlayerPawn_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Arcade_Source_Arcade_Pawns_PlayerPawn_h_18_INCLASS_NO_PURE_DECLS \
	Arcade_Source_Arcade_Pawns_PlayerPawn_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARCADE_API UClass* StaticClass<class APlayerPawn>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Arcade_Source_Arcade_Pawns_PlayerPawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
