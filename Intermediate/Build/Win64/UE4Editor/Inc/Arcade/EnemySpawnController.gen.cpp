// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Arcade/Components/EnemySpawnController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEnemySpawnController() {}
// Cross Module References
	ARCADE_API UScriptStruct* Z_Construct_UScriptStruct_FEnemySpawnInfo();
	UPackage* Z_Construct_UPackage__Script_Arcade();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ARCADE_API UClass* Z_Construct_UClass_AEnemyPawn_NoRegister();
	ARCADE_API UClass* Z_Construct_UClass_UEnemySpawnController_NoRegister();
	ARCADE_API UClass* Z_Construct_UClass_UEnemySpawnController();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
// End Cross Module References
class UScriptStruct* FEnemySpawnInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ARCADE_API uint32 Get_Z_Construct_UScriptStruct_FEnemySpawnInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FEnemySpawnInfo, Z_Construct_UPackage__Script_Arcade(), TEXT("EnemySpawnInfo"), sizeof(FEnemySpawnInfo), Get_Z_Construct_UScriptStruct_FEnemySpawnInfo_Hash());
	}
	return Singleton;
}
template<> ARCADE_API UScriptStruct* StaticStruct<FEnemySpawnInfo>()
{
	return FEnemySpawnInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FEnemySpawnInfo(FEnemySpawnInfo::StaticStruct, TEXT("/Script/Arcade"), TEXT("EnemySpawnInfo"), false, nullptr, nullptr);
static struct FScriptStruct_Arcade_StaticRegisterNativesFEnemySpawnInfo
{
	FScriptStruct_Arcade_StaticRegisterNativesFEnemySpawnInfo()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("EnemySpawnInfo")),new UScriptStruct::TCppStructOps<FEnemySpawnInfo>);
	}
} ScriptStruct_Arcade_StaticRegisterNativesFEnemySpawnInfo;
	struct Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnDelay_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SpawnDelay;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumOfEnemies_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_NumOfEnemies;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Type;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SpawnTransform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnemyClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_EnemyClass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Components/EnemySpawnController.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FEnemySpawnInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_SpawnDelay_MetaData[] = {
		{ "Category", "Enemies" },
		{ "ModuleRelativePath", "Components/EnemySpawnController.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_SpawnDelay = { "SpawnDelay", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEnemySpawnInfo, SpawnDelay), METADATA_PARAMS(Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_SpawnDelay_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_SpawnDelay_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_NumOfEnemies_MetaData[] = {
		{ "Category", "Enemies" },
		{ "ModuleRelativePath", "Components/EnemySpawnController.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_NumOfEnemies = { "NumOfEnemies", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEnemySpawnInfo, NumOfEnemies), METADATA_PARAMS(Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_NumOfEnemies_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_NumOfEnemies_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_Type_MetaData[] = {
		{ "Category", "Enemies" },
		{ "ModuleRelativePath", "Components/EnemySpawnController.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEnemySpawnInfo, Type), METADATA_PARAMS(Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_Type_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_SpawnTransform_MetaData[] = {
		{ "Category", "Enemies" },
		{ "ModuleRelativePath", "Components/EnemySpawnController.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_SpawnTransform = { "SpawnTransform", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEnemySpawnInfo, SpawnTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_SpawnTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_SpawnTransform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_EnemyClass_MetaData[] = {
		{ "Category", "Enemies" },
		{ "ModuleRelativePath", "Components/EnemySpawnController.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_EnemyClass = { "EnemyClass", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEnemySpawnInfo, EnemyClass), Z_Construct_UClass_AEnemyPawn_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_EnemyClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_EnemyClass_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_SpawnDelay,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_NumOfEnemies,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_SpawnTransform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::NewProp_EnemyClass,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Arcade,
		nullptr,
		&NewStructOps,
		"EnemySpawnInfo",
		sizeof(FEnemySpawnInfo),
		alignof(FEnemySpawnInfo),
		Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FEnemySpawnInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FEnemySpawnInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Arcade();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("EnemySpawnInfo"), sizeof(FEnemySpawnInfo), Get_Z_Construct_UScriptStruct_FEnemySpawnInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FEnemySpawnInfo_Hash() { return 3430266032U; }
	void UEnemySpawnController::StaticRegisterNativesUEnemySpawnController()
	{
	}
	UClass* Z_Construct_UClass_UEnemySpawnController_NoRegister()
	{
		return UEnemySpawnController::StaticClass();
	}
	struct Z_Construct_UClass_UEnemySpawnController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnStages_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SpawnStages;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SpawnStages_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEnemySpawnController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_Arcade,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnemySpawnController_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "Components/EnemySpawnController.h" },
		{ "ModuleRelativePath", "Components/EnemySpawnController.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnemySpawnController_Statics::NewProp_SpawnStages_MetaData[] = {
		{ "Category", "Enemies" },
		{ "ModuleRelativePath", "Components/EnemySpawnController.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UEnemySpawnController_Statics::NewProp_SpawnStages = { "SpawnStages", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEnemySpawnController, SpawnStages), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UEnemySpawnController_Statics::NewProp_SpawnStages_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEnemySpawnController_Statics::NewProp_SpawnStages_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UEnemySpawnController_Statics::NewProp_SpawnStages_Inner = { "SpawnStages", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FEnemySpawnInfo, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEnemySpawnController_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnemySpawnController_Statics::NewProp_SpawnStages,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnemySpawnController_Statics::NewProp_SpawnStages_Inner,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEnemySpawnController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEnemySpawnController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEnemySpawnController_Statics::ClassParams = {
		&UEnemySpawnController::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UEnemySpawnController_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UEnemySpawnController_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UEnemySpawnController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEnemySpawnController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEnemySpawnController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEnemySpawnController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEnemySpawnController, 2064324041);
	template<> ARCADE_API UClass* StaticClass<UEnemySpawnController>()
	{
		return UEnemySpawnController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEnemySpawnController(Z_Construct_UClass_UEnemySpawnController, &UEnemySpawnController::StaticClass, TEXT("/Script/Arcade"), TEXT("UEnemySpawnController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEnemySpawnController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
