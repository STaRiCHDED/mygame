// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ARCADE_GameHealthComponent_generated_h
#error "GameHealthComponent.generated.h already included, missing '#pragma once' in GameHealthComponent.h"
#endif
#define ARCADE_GameHealthComponent_generated_h

#define Arcade_Source_Arcade_Components_GameHealthComponent_h_10_DELEGATE \
static inline void FHealthsEndedEvent_DelegateWrapper(const FMulticastScriptDelegate& HealthsEndedEvent) \
{ \
	HealthsEndedEvent.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Arcade_Source_Arcade_Components_GameHealthComponent_h_17_SPARSE_DATA
#define Arcade_Source_Arcade_Components_GameHealthComponent_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetHealths); \
	DECLARE_FUNCTION(execChangeHealths); \
	DECLARE_FUNCTION(execOnPlayerDamaged);


#define Arcade_Source_Arcade_Components_GameHealthComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetHealths); \
	DECLARE_FUNCTION(execChangeHealths); \
	DECLARE_FUNCTION(execOnPlayerDamaged);


#define Arcade_Source_Arcade_Components_GameHealthComponent_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGameHealthComponent(); \
	friend struct Z_Construct_UClass_UGameHealthComponent_Statics; \
public: \
	DECLARE_CLASS(UGameHealthComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcade"), NO_API) \
	DECLARE_SERIALIZER(UGameHealthComponent)


#define Arcade_Source_Arcade_Components_GameHealthComponent_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUGameHealthComponent(); \
	friend struct Z_Construct_UClass_UGameHealthComponent_Statics; \
public: \
	DECLARE_CLASS(UGameHealthComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcade"), NO_API) \
	DECLARE_SERIALIZER(UGameHealthComponent)


#define Arcade_Source_Arcade_Components_GameHealthComponent_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGameHealthComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameHealthComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGameHealthComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameHealthComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGameHealthComponent(UGameHealthComponent&&); \
	NO_API UGameHealthComponent(const UGameHealthComponent&); \
public:


#define Arcade_Source_Arcade_Components_GameHealthComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGameHealthComponent(UGameHealthComponent&&); \
	NO_API UGameHealthComponent(const UGameHealthComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGameHealthComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameHealthComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGameHealthComponent)


#define Arcade_Source_Arcade_Components_GameHealthComponent_h_17_PRIVATE_PROPERTY_OFFSET
#define Arcade_Source_Arcade_Components_GameHealthComponent_h_14_PROLOG
#define Arcade_Source_Arcade_Components_GameHealthComponent_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcade_Source_Arcade_Components_GameHealthComponent_h_17_PRIVATE_PROPERTY_OFFSET \
	Arcade_Source_Arcade_Components_GameHealthComponent_h_17_SPARSE_DATA \
	Arcade_Source_Arcade_Components_GameHealthComponent_h_17_RPC_WRAPPERS \
	Arcade_Source_Arcade_Components_GameHealthComponent_h_17_INCLASS \
	Arcade_Source_Arcade_Components_GameHealthComponent_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Arcade_Source_Arcade_Components_GameHealthComponent_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcade_Source_Arcade_Components_GameHealthComponent_h_17_PRIVATE_PROPERTY_OFFSET \
	Arcade_Source_Arcade_Components_GameHealthComponent_h_17_SPARSE_DATA \
	Arcade_Source_Arcade_Components_GameHealthComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Arcade_Source_Arcade_Components_GameHealthComponent_h_17_INCLASS_NO_PURE_DECLS \
	Arcade_Source_Arcade_Components_GameHealthComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARCADE_API UClass* StaticClass<class UGameHealthComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Arcade_Source_Arcade_Components_GameHealthComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
