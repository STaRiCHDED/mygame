// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Arcade/ArcadeGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeArcadeGameModeBase() {}
// Cross Module References
	ARCADE_API UFunction* Z_Construct_UDelegateFunction_Arcade_GameOverEvent__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_Arcade();
	ARCADE_API UClass* Z_Construct_UClass_AArcadeGameModeBase_NoRegister();
	ARCADE_API UClass* Z_Construct_UClass_AArcadeGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	ARCADE_API UClass* Z_Construct_UClass_UGameHealthComponent_NoRegister();
	ARCADE_API UClass* Z_Construct_UClass_UEnemySpawnController_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_Arcade_GameOverEvent__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Arcade_GameOverEvent__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ArcadeGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_Arcade_GameOverEvent__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_Arcade, nullptr, "GameOverEvent__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_Arcade_GameOverEvent__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Arcade_GameOverEvent__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_Arcade_GameOverEvent__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_Arcade_GameOverEvent__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(AArcadeGameModeBase::execEndGame)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EndGame();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AArcadeGameModeBase::execAddPoints)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_Points);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddPoints(Z_Param_Points);
		P_NATIVE_END;
	}
	void AArcadeGameModeBase::StaticRegisterNativesAArcadeGameModeBase()
	{
		UClass* Class = AArcadeGameModeBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddPoints", &AArcadeGameModeBase::execAddPoints },
			{ "EndGame", &AArcadeGameModeBase::execEndGame },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AArcadeGameModeBase_AddPoints_Statics
	{
		struct ArcadeGameModeBase_eventAddPoints_Parms
		{
			int32 Points;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Points;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_AArcadeGameModeBase_AddPoints_Statics::NewProp_Points = { "Points", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ArcadeGameModeBase_eventAddPoints_Parms, Points), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AArcadeGameModeBase_AddPoints_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AArcadeGameModeBase_AddPoints_Statics::NewProp_Points,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AArcadeGameModeBase_AddPoints_Statics::Function_MetaDataParams[] = {
		{ "Category", "Game" },
		{ "ModuleRelativePath", "ArcadeGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AArcadeGameModeBase_AddPoints_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AArcadeGameModeBase, nullptr, "AddPoints", nullptr, nullptr, sizeof(ArcadeGameModeBase_eventAddPoints_Parms), Z_Construct_UFunction_AArcadeGameModeBase_AddPoints_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AArcadeGameModeBase_AddPoints_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AArcadeGameModeBase_AddPoints_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AArcadeGameModeBase_AddPoints_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AArcadeGameModeBase_AddPoints()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AArcadeGameModeBase_AddPoints_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AArcadeGameModeBase_EndGame_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AArcadeGameModeBase_EndGame_Statics::Function_MetaDataParams[] = {
		{ "Category", "Game" },
		{ "ModuleRelativePath", "ArcadeGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AArcadeGameModeBase_EndGame_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AArcadeGameModeBase, nullptr, "EndGame", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AArcadeGameModeBase_EndGame_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AArcadeGameModeBase_EndGame_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AArcadeGameModeBase_EndGame()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AArcadeGameModeBase_EndGame_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AArcadeGameModeBase_NoRegister()
	{
		return AArcadeGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_AArcadeGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GameOver_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_GameOver;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Time_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Time;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GamePoints_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_GamePoints;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HealthsComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HealthsComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnemySpawnController_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EnemySpawnController;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AArcadeGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Arcade,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AArcadeGameModeBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AArcadeGameModeBase_AddPoints, "AddPoints" }, // 1757465918
		{ &Z_Construct_UFunction_AArcadeGameModeBase_EndGame, "EndGame" }, // 3567316899
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcadeGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "ArcadeGameModeBase.h" },
		{ "ModuleRelativePath", "ArcadeGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_GameOver_MetaData[] = {
		{ "Category", "Game" },
		{ "ModuleRelativePath", "ArcadeGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_GameOver = { "GameOver", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcadeGameModeBase, GameOver), Z_Construct_UDelegateFunction_Arcade_GameOverEvent__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_GameOver_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_GameOver_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_Time_MetaData[] = {
		{ "Category", "Game" },
		{ "Comment", "//??????\n" },
		{ "ModuleRelativePath", "ArcadeGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_Time = { "Time", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcadeGameModeBase, Time), METADATA_PARAMS(Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_Time_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_Time_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_GamePoints_MetaData[] = {
		{ "Category", "Game" },
		{ "Comment", "//????\n" },
		{ "ModuleRelativePath", "ArcadeGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_GamePoints = { "GamePoints", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcadeGameModeBase, GamePoints), METADATA_PARAMS(Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_GamePoints_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_GamePoints_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_HealthsComponent_MetaData[] = {
		{ "Category", "Game Health" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "ArcadeGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_HealthsComponent = { "HealthsComponent", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcadeGameModeBase, HealthsComponent), Z_Construct_UClass_UGameHealthComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_HealthsComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_HealthsComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_EnemySpawnController_MetaData[] = {
		{ "Category", "Enemies" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "ArcadeGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_EnemySpawnController = { "EnemySpawnController", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcadeGameModeBase, EnemySpawnController), Z_Construct_UClass_UEnemySpawnController_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_EnemySpawnController_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_EnemySpawnController_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AArcadeGameModeBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_GameOver,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_Time,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_GamePoints,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_HealthsComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcadeGameModeBase_Statics::NewProp_EnemySpawnController,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AArcadeGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AArcadeGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AArcadeGameModeBase_Statics::ClassParams = {
		&AArcadeGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AArcadeGameModeBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AArcadeGameModeBase_Statics::PropPointers),
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AArcadeGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AArcadeGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AArcadeGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AArcadeGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AArcadeGameModeBase, 371667799);
	template<> ARCADE_API UClass* StaticClass<AArcadeGameModeBase>()
	{
		return AArcadeGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AArcadeGameModeBase(Z_Construct_UClass_AArcadeGameModeBase, &AArcadeGameModeBase::StaticClass, TEXT("/Script/Arcade"), TEXT("AArcadeGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AArcadeGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
