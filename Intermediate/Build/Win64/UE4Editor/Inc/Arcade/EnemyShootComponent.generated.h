// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ARCADE_EnemyShootComponent_generated_h
#error "EnemyShootComponent.generated.h already included, missing '#pragma once' in EnemyShootComponent.h"
#endif
#define ARCADE_EnemyShootComponent_generated_h

#define Arcade_Source_Arcade_Components_EnemyShootComponent_h_13_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FEnemyShootInfo_Statics; \
	ARCADE_API static class UScriptStruct* StaticStruct();


template<> ARCADE_API UScriptStruct* StaticStruct<struct FEnemyShootInfo>();

#define Arcade_Source_Arcade_Components_EnemyShootComponent_h_29_SPARSE_DATA
#define Arcade_Source_Arcade_Components_EnemyShootComponent_h_29_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execStopShooting); \
	DECLARE_FUNCTION(execStartShooting);


#define Arcade_Source_Arcade_Components_EnemyShootComponent_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execStopShooting); \
	DECLARE_FUNCTION(execStartShooting);


#define Arcade_Source_Arcade_Components_EnemyShootComponent_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEnemyShootComponent(); \
	friend struct Z_Construct_UClass_UEnemyShootComponent_Statics; \
public: \
	DECLARE_CLASS(UEnemyShootComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcade"), NO_API) \
	DECLARE_SERIALIZER(UEnemyShootComponent)


#define Arcade_Source_Arcade_Components_EnemyShootComponent_h_29_INCLASS \
private: \
	static void StaticRegisterNativesUEnemyShootComponent(); \
	friend struct Z_Construct_UClass_UEnemyShootComponent_Statics; \
public: \
	DECLARE_CLASS(UEnemyShootComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcade"), NO_API) \
	DECLARE_SERIALIZER(UEnemyShootComponent)


#define Arcade_Source_Arcade_Components_EnemyShootComponent_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEnemyShootComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEnemyShootComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEnemyShootComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnemyShootComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEnemyShootComponent(UEnemyShootComponent&&); \
	NO_API UEnemyShootComponent(const UEnemyShootComponent&); \
public:


#define Arcade_Source_Arcade_Components_EnemyShootComponent_h_29_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEnemyShootComponent(UEnemyShootComponent&&); \
	NO_API UEnemyShootComponent(const UEnemyShootComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEnemyShootComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnemyShootComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UEnemyShootComponent)


#define Arcade_Source_Arcade_Components_EnemyShootComponent_h_29_PRIVATE_PROPERTY_OFFSET
#define Arcade_Source_Arcade_Components_EnemyShootComponent_h_26_PROLOG
#define Arcade_Source_Arcade_Components_EnemyShootComponent_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcade_Source_Arcade_Components_EnemyShootComponent_h_29_PRIVATE_PROPERTY_OFFSET \
	Arcade_Source_Arcade_Components_EnemyShootComponent_h_29_SPARSE_DATA \
	Arcade_Source_Arcade_Components_EnemyShootComponent_h_29_RPC_WRAPPERS \
	Arcade_Source_Arcade_Components_EnemyShootComponent_h_29_INCLASS \
	Arcade_Source_Arcade_Components_EnemyShootComponent_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Arcade_Source_Arcade_Components_EnemyShootComponent_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcade_Source_Arcade_Components_EnemyShootComponent_h_29_PRIVATE_PROPERTY_OFFSET \
	Arcade_Source_Arcade_Components_EnemyShootComponent_h_29_SPARSE_DATA \
	Arcade_Source_Arcade_Components_EnemyShootComponent_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	Arcade_Source_Arcade_Components_EnemyShootComponent_h_29_INCLASS_NO_PURE_DECLS \
	Arcade_Source_Arcade_Components_EnemyShootComponent_h_29_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARCADE_API UClass* StaticClass<class UEnemyShootComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Arcade_Source_Arcade_Components_EnemyShootComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
