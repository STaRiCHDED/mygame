// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ARCADE_EnemySpawnController_generated_h
#error "EnemySpawnController.generated.h already included, missing '#pragma once' in EnemySpawnController.h"
#endif
#define ARCADE_EnemySpawnController_generated_h

#define Arcade_Source_Arcade_Components_EnemySpawnController_h_13_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FEnemySpawnInfo_Statics; \
	ARCADE_API static class UScriptStruct* StaticStruct();


template<> ARCADE_API UScriptStruct* StaticStruct<struct FEnemySpawnInfo>();

#define Arcade_Source_Arcade_Components_EnemySpawnController_h_39_SPARSE_DATA
#define Arcade_Source_Arcade_Components_EnemySpawnController_h_39_RPC_WRAPPERS
#define Arcade_Source_Arcade_Components_EnemySpawnController_h_39_RPC_WRAPPERS_NO_PURE_DECLS
#define Arcade_Source_Arcade_Components_EnemySpawnController_h_39_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEnemySpawnController(); \
	friend struct Z_Construct_UClass_UEnemySpawnController_Statics; \
public: \
	DECLARE_CLASS(UEnemySpawnController, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcade"), NO_API) \
	DECLARE_SERIALIZER(UEnemySpawnController)


#define Arcade_Source_Arcade_Components_EnemySpawnController_h_39_INCLASS \
private: \
	static void StaticRegisterNativesUEnemySpawnController(); \
	friend struct Z_Construct_UClass_UEnemySpawnController_Statics; \
public: \
	DECLARE_CLASS(UEnemySpawnController, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcade"), NO_API) \
	DECLARE_SERIALIZER(UEnemySpawnController)


#define Arcade_Source_Arcade_Components_EnemySpawnController_h_39_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEnemySpawnController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEnemySpawnController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEnemySpawnController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnemySpawnController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEnemySpawnController(UEnemySpawnController&&); \
	NO_API UEnemySpawnController(const UEnemySpawnController&); \
public:


#define Arcade_Source_Arcade_Components_EnemySpawnController_h_39_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEnemySpawnController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEnemySpawnController(UEnemySpawnController&&); \
	NO_API UEnemySpawnController(const UEnemySpawnController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEnemySpawnController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnemySpawnController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEnemySpawnController)


#define Arcade_Source_Arcade_Components_EnemySpawnController_h_39_PRIVATE_PROPERTY_OFFSET
#define Arcade_Source_Arcade_Components_EnemySpawnController_h_36_PROLOG
#define Arcade_Source_Arcade_Components_EnemySpawnController_h_39_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcade_Source_Arcade_Components_EnemySpawnController_h_39_PRIVATE_PROPERTY_OFFSET \
	Arcade_Source_Arcade_Components_EnemySpawnController_h_39_SPARSE_DATA \
	Arcade_Source_Arcade_Components_EnemySpawnController_h_39_RPC_WRAPPERS \
	Arcade_Source_Arcade_Components_EnemySpawnController_h_39_INCLASS \
	Arcade_Source_Arcade_Components_EnemySpawnController_h_39_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Arcade_Source_Arcade_Components_EnemySpawnController_h_39_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcade_Source_Arcade_Components_EnemySpawnController_h_39_PRIVATE_PROPERTY_OFFSET \
	Arcade_Source_Arcade_Components_EnemySpawnController_h_39_SPARSE_DATA \
	Arcade_Source_Arcade_Components_EnemySpawnController_h_39_RPC_WRAPPERS_NO_PURE_DECLS \
	Arcade_Source_Arcade_Components_EnemySpawnController_h_39_INCLASS_NO_PURE_DECLS \
	Arcade_Source_Arcade_Components_EnemySpawnController_h_39_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARCADE_API UClass* StaticClass<class UEnemySpawnController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Arcade_Source_Arcade_Components_EnemySpawnController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
