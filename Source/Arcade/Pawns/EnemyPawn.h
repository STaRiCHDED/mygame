// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/BoxComponent.h"
#include "Arcade/Components/EnemyShootComponent.h"
#include "Arcade/Components/HealthComponent.h"
#include "EnemyPawn.generated.h"

//USTRUCT(BlueprintType)
//struct FBonusChance
//{
//	GENERATED_BODY()
//
//public:
//
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonus")
//		TSubclassOf<ABonus> BonusClass;
//
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonus")
//		float Chance;
//
//};



UCLASS()
class ARCADE_API AEnemyPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AEnemyPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//void SpawnBonuses();

	UFUNCTION()
	void KillPawn();

	UFUNCTION()
	void OnEnemyOverlap(AActor* OverlapedActor, AActor* OtherActor);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "Pawn")
		void DestroyPawn();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pawn")
		UBoxComponent* PawnCollision;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Pawn")
		UStaticMeshComponent* PawnMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Shooting")
		UEnemyShootComponent* EnemyShootComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Shooting")
		UHealthComponent* HealthComponent;


	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Score")
	int DestroyPoints;

	//UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Bonus")
	//	TArray<FBonusChance> PossibleBonuses;

	//UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Visual")
	//	UParticleSystem* DestroyParticle;
};
