// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawn.h"
#include "Components/StaticMeshComponent.h"
#include "Arcade/Actors/Projectiles/EnemyShootProjectile.h"
#include "Components/InputComponent.h"
#include "GameFramework/PlayerController.h"


// Sets default values
APlayerPawn::APlayerPawn():TouchMoveSensivity(1.f), MoveLimit(FVector2D(600.f,500.f)), Healths(3) //CanRecieveDamage(true)
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("PawnCollision"));
	SetRootComponent(PawnCollision);

	PawnMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PawnMesh"));
	PawnMesh->SetupAttachment(PawnCollision);

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));

	ShootComponent = CreateDefaultSubobject<UShootComponent>(TEXT("ShootComponent"));
}

void APlayerPawn::PossessedBy(AController* NewController)
{
	PlayerController = Cast<APlayerController>(NewController);
}

//bool APlayerPawn::CanBeDamaged_Implementation()
//{
//	UE_LOG(LogTemp, Log, TEXT("Damage Test!!"));
//	return true;
//}

// Called when the game starts or when spawned
void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	
	OnActorBeginOverlap.AddDynamic(this, &APlayerPawn::OnPlayerOverlap);
}

//float APlayerPawn::TakeDamage(float DamageAmount, const FDamageEvent& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
//{
//	if (!CanBeDamaged()) {
//		UE_LOG(LogTemp, Log, TEXT("Can't Be Damaged!!"));
//		return 0.f;//����� �� ���������� ��� ���� � ������ ������(���� �� ���������)
//	}
//
//	Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);//��� ������ ����� �� ����� ��������� ������������ ������,�� ����� ���������� ���������
//
//	//UE_LOG(LogTemp, Log, TEXT("Player Damaged!!"));
//
//	return DamageAmount;
//}

//int APlayerPawn::GetHealths()
//{
//	return Healths;
//}

void APlayerPawn::OnPlayerOverlap(AActor* OverlapedActor, AActor* OtherActor)
{
	Healths += -1;
	PawnDamaged.Broadcast();
	UE_LOG(LogTemp, Log, TEXT("%i Healths"), Healths);
	if (Healths <= 0) {
		Destroy();
	}
	
}


// Called every frame
void APlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	////����������
	//bool Touch;
	//float TouchX, TouchY;
	//PlayerController->GetInputTouchState(ETouchIndex::Touch1, TouchX, TouchY, Touch);

	//if (Touch) {

	//	FVector2D TouchDeltaMove = FVector2D(TouchLocation.X - TouchX, TouchLocation.Y - TouchY);

	//	UE_LOG(LogTemp, Log, TEXT("Touching in %f-%f"), TouchLocation.X - TouchX, TouchLocation.Y - TouchY);
	//	//�������
	//	FVector NewLocation = GetActorLocation();
	//	NewLocation.X = FMath::Clamp(NewLocation.X + TouchDeltaMove.Y,-500.f,500.f);
	//	NewLocation.Y = FMath::Clamp(NewLocation.Y + TouchDeltaMove.X*-1.f, -600.f, 600.f);
	//	//�������� ����.��������
	//	//AddActorLocalOffset(FVector(TouchDeltaMove.Y, TouchDeltaMove.X*-1.f, 0.f));
	//	SetActorLocation(NewLocation);

	//	TouchLocation = FVector2D(TouchX,TouchY);
	//}
}

// Called to bind functionality to input
//���� ����������
void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindTouch(IE_Pressed, this, &APlayerPawn::OnTouchPress);
	//InputComponent->BindTouch(IE_Released, this, &APlayerPawn::OnTouchRelease);

	InputComponent->BindTouch(IE_Repeat, this, &APlayerPawn::OnTouchMove);
}


//����������----------------------------------------------------------------------------------------------
void APlayerPawn::OnTouchMove(ETouchIndex::Type FingerIndex, FVector Location)
{
	FVector2D TouchDeltaMove = FVector2D(TouchLocation.X - Location.X, TouchLocation.Y - Location.Y);

	TouchDeltaMove = TouchDeltaMove * TouchMoveSensivity;
	//UE_LOG(LogTemp, Log, TEXT("Touching in %f-%f"), TouchLocation.X - TouchX, TouchLocation.Y - TouchY);
	//�������
	FVector NewLocation = GetActorLocation();
	NewLocation.X = FMath::Clamp(NewLocation.X + TouchDeltaMove.Y, -MoveLimit.Y, MoveLimit.Y);
	NewLocation.Y = FMath::Clamp(NewLocation.Y + TouchDeltaMove.X * -1.f, -MoveLimit.X, MoveLimit.X);
	//�������� ����.��������
	//AddActorLocalOffset(FVector(TouchDeltaMove.Y, TouchDeltaMove.X*-1.f, 0.f));
	SetActorLocation(NewLocation);

	TouchLocation = FVector2D(Location.X, Location.Y);
}



void APlayerPawn::OnTouchPress(ETouchIndex::Type FingerIndex,FVector Location)
{
	//UE_LOG(LogTemp, Log, TEXT("Touch Press : %s"), *TouchLocation.ToString());
	TouchLocation = FVector2D(Location.X, Location.Y);
	//TouchLocation = FVector2D(Location);
	//Touching = true;
}

/*void APlayerPawn::OnTouchRelease(ETouchIndex::Type FingerIndex, FVector Location)
{
	UE_LOG(LogTemp, Log, TEXT("Touch Release : %s"), *TouchLocation.ToString());
	Touching = false;
}*/
