// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/BoxComponent.h"
#include "Camera/CameraComponent.h"
#include "Arcade/Components/ShootComponent.h"
#include "PlayerPawn.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPawnDamagedEvent);


UCLASS()
class ARCADE_API APlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//virtual float TakeDamage(float Damage, const FDamageEvent& DamageEvent, AController* InstigatedBy, AActor* DamageCauser) override;

	void OnTouchMove(ETouchIndex::Type FingerIndex, FVector Location);

	void OnTouchPress(ETouchIndex::Type FingerIndex, FVector Location);

	virtual void PossessedBy(AController* NewController) override;

	APlayerController* PlayerController;

	FVector2D MoveLimit;

	UPROPERTY(EditAnywhere, Category = "Game Health")
	int Healths;

	UPROPERTY(EditAnywhere, Category = "Game Health")
	int Score;

private:

	FVector2D TouchLocation;

	bool Touching;

	//UMaterialInterface* PawnMaterial;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	//UFUNCTION(BlueprintPure, BlueprintNativeEvent, Category = "Healths")
	//bool CanBeDamaged();
	//bool CanBeDamaged_Implementation();

	//UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Healths")
	//void ExplodePawn();
	//void ExplodePawn_Implementation();

	//UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Healths")
	//void RecoverPawn();
	//void RecoverPawn_Implementation();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pawn")
	UBoxComponent* PawnCollision;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Pawn")
	UStaticMeshComponent* PawnMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pawn")
	UCameraComponent* PawnCamera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Shooting")
	UShootComponent* ShootComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Controls")
	float TouchMoveSensivity;



	//UFUNCTION(BlueprintCallable, Category = "Game Health")
	//void ChangeHealths(int ByValue);

	//UFUNCTION(BlueprintCallable, Category = "Game Health")
	//int GetHealths();


	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Shooting")
	//UGameHealthComponent* GameHealthComponent;

	UPROPERTY(BlueprintAssignable, Category = "Healths")
	FPawnDamagedEvent PawnDamaged;

	//UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pawn")
	//UMaterialInterface* RecoverMaterial;

	//UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Visual")
	//UParticleSystem* DestroyParticle;

	UFUNCTION()
		void OnPlayerOverlap(AActor* OverlapedActor, AActor* OtherActor);
};
