// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Components/EnemySpawnController.h"
#include "Components/GameHealthComponent.h"
#include "Components/ShootComponent.h"
#include "ArcadeGameModeBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FGameOverEvent);


UCLASS()
class ARCADE_API AArcadeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

		AArcadeGameModeBase();

	virtual void BeginPlay() override;

protected:

	FTimerHandle RecoverTimer;

	FTimerHandle IncreaseDifficultyTimer;

	FTimerHandle HUDTimer;

	void SetOneSec();

	bool IsGameOver;

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Enemies")
	UEnemySpawnController* EnemySpawnController;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game Health")
	UGameHealthComponent* HealthsComponent;


	UFUNCTION(BlueprintCallable, Category = "Game")
	void AddPoints(int Points);

	//����
	UPROPERTY(BlueprintReadOnly, Category = "Game")
	int GamePoints;

	//������
	UPROPERTY(BlueprintReadOnly, Category = "Game")
	int Time;


	UPROPERTY(BlueprintAssignable, Category = "Game")
	FGameOverEvent GameOver;

	UFUNCTION(BlueprintCallable, Category = "Game")
	void EndGame();

	//UFUNCTION(BlueprintCallable, Category = "Game")
	//void IncreaseDifficulty();

	//UFUNCTION(BlueprintCallable, Category = "Game")
	//void AddPoints(int Points);

	//UFUNCTION(BlueprintCallable, Category = "Game")
	//bool ChangeShootLevel(bool Up);

	//UPROPERTY(BlueprintReadWrite, Category = "Game")
	//float PlayerRecoverTime;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game")
	//float IncreaseDifficultyPeriod;

	//UPROPERTY(BlueprintReadOnly, Category = "Game")
	//class APlayerPawn* PlayerPawn;

	//UPROPERTY(BlueprintReadOnly, Category = "Game")
	//int GamePoints;

	//UPROPERTY(EditDefaultsOnly, Category = "Shooting")
	//TArray<FShootInfoLevel> ShootInfoLevels;

	//UPROPERTY(BlueprintReadOnly, Category = "Shooting")
	//int CurrentShootLevel;
};
