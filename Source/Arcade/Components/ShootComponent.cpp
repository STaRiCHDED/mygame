// Fill out your copyright notice in the Description page of Project Settings.


#include "ShootComponent.h"
#include "Engine/World.h"
#include "TimerManager.h"

// Sets default values for this component's properties
UShootComponent::UShootComponent() :ShootPeriod(1.f)
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.


	//PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UShootComponent::BeginPlay()
{
	Super::BeginPlay();

	StartShooting();

}

void UShootComponent::Shoot()
{
	//UE_LOG(LogTemp, Log, TEXT("Shoot!!!"));

	for (FShootInfo ShootInfo : ShootInfos) {



		//for(int i=0;i<ShootInfos.Num();i++){}

		FActorSpawnParameters SpawnParameters;

		SpawnParameters.Owner = GetOwner();
		//SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;// ����� ������

		//FVector SpawnLocation = GetOwner()->GetActorLocation();

		//FVector SpawnLocation = GetOwner()->GetActorLocation() + ShootInfo.Offset.RotateAngleAxis(GetOwner()->GetActorRotation().Yaw, FVector::UpVector);
																																	//FVector(0.f, 0.f, 1.f)

		FVector SpawnLocation = GetOwner()->GetActorLocation() + GetOwner()->GetActorRotation().RotateVector(ShootInfo.Offset);
		FRotator SpawnRotation = GetOwner()->GetActorRotation();

		SpawnRotation.Add(0.f, ShootInfo.Angle, 0.f);

		//FTransform SpawnTransform = FTransform();
		//SpawnTransform.SetLocation(GetOwner()->GetActorLocation());

		//SpawnTransform.SetRotation(SpawnRotation);


		AShootProjectile* Projectile = GetWorld()->SpawnActor<AShootProjectile>(ShootInfo.ProjectileClass, SpawnLocation, SpawnRotation, SpawnParameters);
		if(Projectile) Projectile->Damage = ShootInfo.Damage;
	}
}

void UShootComponent::StartShooting()
{
	GetWorld()->GetTimerManager().SetTimer(ShootingTimer, this, &UShootComponent::Shoot, ShootPeriod, true, ShootPeriod);
}

void UShootComponent::StopShooting()
{
	GetWorld()->GetTimerManager().ClearTimer(ShootingTimer);
}


