// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemySpawnController.h"
#include "Engine/World.h"
#include "TimerManager.h"

// Called when the game starts
void UEnemySpawnController::BeginPlay()
{
	Super::BeginPlay();

	Random.GenerateNewSeed();
	StartSpawnStage();

}

void UEnemySpawnController::Deactivate()
{
	Super::Deactivate();

	GetWorld()->GetTimerManager().ClearTimer(ChangeStageTimer);
	GetWorld()->GetTimerManager().ClearTimer(EnemySpawnTimer);
}

void UEnemySpawnController::StartSpawnStage()
{
	SpawnStage = SpawnStages[Random.RandRange(0, SpawnStages.Num() - 1)];

	SpawnEnemy();

	//float ChangeStageTime = Random.RandRange(StageMinDelay, StageMaxDelay);
	if (SpawnStage.Type == 0) {
		//float ChangeStageTime = Random.RandRange(StageMinDelay, StageMaxDelay);
		float ChangeStageTime = SpawnStage.SpawnDelay + 5.f;
		GetWorld()->GetTimerManager().SetTimer(ChangeStageTimer, this, &UEnemySpawnController::StartSpawnStage, ChangeStageTime, false);
	}
	if (SpawnStage.Type == 1) {
		float ChangeStageTime = SpawnStage.SpawnDelay + 5.f;
		GetWorld()->GetTimerManager().SetTimer(ChangeStageTimer, this, &UEnemySpawnController::StartSpawnStage, ChangeStageTime, false);
	}
	if (SpawnStage.Type == 2) {
		float ChangeStageTime = SpawnStage.SpawnDelay + 5.f;
		GetWorld()->GetTimerManager().SetTimer(ChangeStageTimer, this, &UEnemySpawnController::StartSpawnStage, ChangeStageTime, false);
	}
	if (SpawnStage.Type == 3) {
		float ChangeStageTime = SpawnStage.SpawnDelay + 5.f;
		GetWorld()->GetTimerManager().SetTimer(ChangeStageTimer, this, &UEnemySpawnController::StartSpawnStage, ChangeStageTime, false);
	}
	if (SpawnStage.Type == 4) {
		float ChangeStageTime = SpawnStage.SpawnDelay + 5.f;
		GetWorld()->GetTimerManager().SetTimer(ChangeStageTimer, this, &UEnemySpawnController::StartSpawnStage, ChangeStageTime, false);
	}
	//GetWorld()->GetTimerManager().SetTimer(ChangeStageTimer, this, &UEnemySpawnController::StartSpawnStage, ChangeStageTime, true);
}


void UEnemySpawnController::SpawnEnemy()
{
	if (SpawnStage.Type == 0) {
		float X = 0; float Y = 0;
		for (int i = 0; i < SpawnStage.NumOfEnemies; i++) {
			if (SpawnStage.NumOfEnemies == 1) {
				FActorSpawnParameters SpawnParameters;
				GetWorld()->SpawnActor<AEnemyPawn>(SpawnStage.EnemyClass, SpawnStage.SpawnTransform, SpawnParameters);
			}
			if ((SpawnStage.NumOfEnemies % 2 == 1)&&(SpawnStage.NumOfEnemies!=1)){
				FActorSpawnParameters SpawnParameters;
				GetWorld()->SpawnActor<AEnemyPawn>(SpawnStage.EnemyClass, FVector(300.f + X, -1000.f - Y, 0.f), FRotator(0.f, 90.f, 0.f), SpawnParameters);
				X += 100.f; Y += 100.f;
			}
			if (SpawnStage.NumOfEnemies % 2 == 0) {
				FActorSpawnParameters SpawnParameters;
				GetWorld()->SpawnActor<AEnemyPawn>(SpawnStage.EnemyClass, FVector(300.f + X, 1000.f + Y, 0.f), FRotator(0.f, -90.f, 0.f), SpawnParameters);
				X += 100.f; Y += 100.f;
			}
		}
	}
	if (SpawnStage.Type == 1) {
		float Y = 0;
		for (int i = 0; i < SpawnStage.NumOfEnemies;i++) {
			FActorSpawnParameters SpawnParameters;
			GetWorld()->SpawnActor<AEnemyPawn>(SpawnStage.EnemyClass,FVector(600.f,500.f-Y,0.f), FRotator(0.f,-180.f,0.f), SpawnParameters);
			Y += 100.f;
		}
	}
	if (SpawnStage.Type == 2) {
		float X = 0; float Y = 0;
		for (int i = 0; i < SpawnStage.NumOfEnemies; i++) {
			FActorSpawnParameters SpawnParameters;
			GetWorld()->SpawnActor<AEnemyPawn>(SpawnStage.EnemyClass, FVector(600.f+X,-500.f + Y, 0.f), FRotator(0.f, -180.f, 0.f), SpawnParameters);
			X += 100.f; Y += 100.f;
		}
	}
	if (SpawnStage.Type == 3) {
		float X = 0; float Y = 0;
		for (int i = 0; i < SpawnStage.NumOfEnemies; i++) {
			FActorSpawnParameters SpawnParameters;
			GetWorld()->SpawnActor<AEnemyPawn>(SpawnStage.EnemyClass, FVector(600.f + X, 500.f - Y, 0.f), FRotator(0.f, -180.f, 0.f), SpawnParameters);
			X += 100.f; Y += 100.f;
		}
	}
	if (SpawnStage.Type == 4) {
		float X = 0; float Y = 0;
		FActorSpawnParameters SpawnParameters;
		GetWorld()->SpawnActor<AEnemyPawn>(SpawnStage.EnemyClass, FVector(600.f, 0.f, 0.f), FRotator(0.f, -180.f, 0.f), SpawnParameters);
		for (int i = 0; i < (SpawnStage.NumOfEnemies-1)/2; i++) {
			X += 100.f; Y += 100.f;
			FActorSpawnParameters SpawnParameters1;
			GetWorld()->SpawnActor<AEnemyPawn>(SpawnStage.EnemyClass, FVector(600.f + X, 0.f - Y, 0.f), FRotator(0.f, -180.f, 0.f), SpawnParameters1);
			GetWorld()->SpawnActor<AEnemyPawn>(SpawnStage.EnemyClass, FVector(600.f + X, 0.f + Y, 0.f), FRotator(0.f, -180.f, 0.f), SpawnParameters1);
		}
	}
}
