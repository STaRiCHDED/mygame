// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"
#include "GameFramework/Actor.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent():Health(100)
{

}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();
	//�������� � ����������
	GetOwner()->OnTakeAnyDamage.AddDynamic(this,&UHealthComponent::OnOwnerDamaged);
	
}

void UHealthComponent::OnOwnerDamaged(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* EventInstigator, AActor* DamageCauser)
{
	ChangeHealth(-Damage);
}

void UHealthComponent::ChangeHealth(float Value)
{
	Health += Value;
	UE_LOG(LogTemp, Log, TEXT("Ship health:%f"),Health);
	if (Health <= 0.f) {
		GetOwner()->OnTakeAnyDamage.RemoveDynamic(this, &UHealthComponent::OnOwnerDamaged);//������� ����������
		//UE_LOG(LogTemp, Log, TEXT("Ship destroed!!!"));
		// primer
		//GetOwner()->Destroy();
		OnHealthEnded.Broadcast();
	}
}

float UHealthComponent::GetHealth()
{
	return Health;
}



