// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyShootComponent.h"
#include "Engine/World.h"
#include "TimerManager.h"

// Sets default values for this component's properties
UEnemyShootComponent::UEnemyShootComponent() :ShootPeriod(1.f)
{
}


// Called when the game starts
void UEnemyShootComponent::BeginPlay()
{
	Super::BeginPlay();

	StartShooting();

}

void UEnemyShootComponent::Shoot()
{
	//UE_LOG(LogTemp, Log, TEXT("Shoot!!!"));

	for (FEnemyShootInfo ShootInfo : EnemyShootInfos) {

		FActorSpawnParameters SpawnParameters;

		SpawnParameters.Owner = GetOwner();


		FVector SpawnLocation = GetOwner()->GetActorLocation() + GetOwner()->GetActorRotation().RotateVector(ShootInfo.EnemyOffset);
		FRotator SpawnRotation = GetOwner()->GetActorRotation();

		SpawnRotation.Add(0.f, ShootInfo.EnemyAngle, 0.f);




		AEnemyShootProjectile* Projectile = GetWorld()->SpawnActor<AEnemyShootProjectile>(ShootInfo.EnemyProjectileClass, SpawnLocation, SpawnRotation, SpawnParameters);
	}
}

void UEnemyShootComponent::StartShooting()
{
	GetWorld()->GetTimerManager().SetTimer(ShootingTimer, this, &UEnemyShootComponent::Shoot, ShootPeriod, true, ShootPeriod);
}

void UEnemyShootComponent::StopShooting()
{
	GetWorld()->GetTimerManager().ClearTimer(ShootingTimer);
}


// Called every frame
//void UEnemyShootComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
//{
//	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
//
//	// ...
//}


