// Fill out your copyright notice in the Description page of Project Settings.


#include "GameHealthComponent.h"
#include "GameFramework/Pawn.h"
#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UGameHealthComponent::UGameHealthComponent() :
	Healths(3)
{
}

// Called when the game starts
void UGameHealthComponent::BeginPlay()
{
	Super::BeginPlay();
	APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(this, 0);
	APlayerPawn* Player= Cast<APlayerPawn>(PlayerPawn);
	Player->PawnDamaged.AddDynamic(this, &UGameHealthComponent::OnPlayerDamaged);
}

//void UGameHealthComponent::OnPlayerDamaged(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* EventInstigator, AActor* DamageCauser)
void UGameHealthComponent::OnPlayerDamaged()
{
	ChangeHealths(-1);
}

void UGameHealthComponent::ChangeHealths(int ByValue)
{
	Healths += ByValue;

	//HealthsChanged.Broadcast();

	if (Healths <= 0) {
		HealthsEnded.Broadcast();
	}
	UE_LOG(LogTemp, Log, TEXT("Health changed: %i"), Healths);
}

int UGameHealthComponent::GetHealths()
{
	return Healths;
}

