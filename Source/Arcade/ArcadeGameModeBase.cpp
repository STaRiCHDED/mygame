// Copyright Epic Games, Inc. All Rights Reserved.


#include "ArcadeGameModeBase.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"
#include "Pawns/PlayerPawn.h"

AArcadeGameModeBase::AArcadeGameModeBase() : GamePoints(0),Time(0)
{
	EnemySpawnController = CreateDefaultSubobject<UEnemySpawnController>(TEXT("EnemySpawnController"));
	HealthsComponent = CreateDefaultSubobject<UGameHealthComponent>(TEXT("HealthsComponent"));
}

void AArcadeGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	HealthsComponent->HealthsEnded.AddDynamic(this, &AArcadeGameModeBase::EndGame);

	//PlayerPawn = Cast<APlayerPawn>(UGameplayStatics::GetPlayerPawn(this, 0));
	//if (!PlayerPawn) return;

	//ChangeShootLevel(true);

	//PlayerPawn->PawnDamaged.AddDynamic(this, &AArcadeGameModeBase::ExplodePawn);

	//GetWorld()->GetTimerManager().SetTimer(IncreaseDifficultyTimer, this, &AArcadeGameModeBase::IncreaseDifficulty, IncreaseDifficultyPeriod, true);

	GetWorld()->GetTimerManager().SetTimer(HUDTimer, this, &AArcadeGameModeBase::SetOneSec, 1.f, true);
}

void AArcadeGameModeBase::SetOneSec()
{
	Time += 1;
}


void AArcadeGameModeBase::EndGame()
{
	//IsGameOver = true;

	EnemySpawnController->SetActive(false);

	//GameOver.Broadcast();

	//UGameplayStatics::GetPlayerPawn(this, 0)->Destroy();

	UE_LOG(LogTemp, Log, TEXT("GAME OVER!!!"));

	GetWorld()->GetTimerManager().ClearTimer(HUDTimer);

	SetPause(UGameplayStatics::GetPlayerController(this, 0), false);
}


void AArcadeGameModeBase::AddPoints(int Points)
{
	GamePoints += Points;
	UE_LOG(LogTemp, Log, TEXT("Score:%d"), GamePoints);
}