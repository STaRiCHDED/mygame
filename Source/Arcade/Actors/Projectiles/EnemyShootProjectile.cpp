// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyShootProjectile.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/DamageType.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AEnemyShootProjectile::AEnemyShootProjectile(): ProjectileSpeed(200.f)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Collision = CreateDefaultSubobject<USphereComponent>(TEXT("ProjectileCollision"));
	RootComponent = Collision;
	Collision->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Collision);
	Mesh->SetCollisionProfileName("NoCollision");


}

// Called when the game starts or when spawned
void AEnemyShootProjectile::BeginPlay()
{
	Super::BeginPlay();

	if (GetOwner())
	{
		UBoxComponent* OwnerCollision = GetOwner()->FindComponentByClass<UBoxComponent>();
		Collision->IgnoreComponentWhenMoving(OwnerCollision, true);
		OwnerCollision->IgnoreComponentWhenMoving(Collision, true);

		Collision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}

	//Collision->OnComponentBeginOverlap.AddDynamic(this, &AEnemyShootProjectile::OnProjectileOverlap);
	OnActorBeginOverlap.AddDynamic(this, &AEnemyShootProjectile::OnProjectileOverlap);
}

void AEnemyShootProjectile::OnProjectileOverlap(AActor* OverlapedActor, AActor* OtherActor)
{
	Destroy();
}


//// Called every frame

void AEnemyShootProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AddActorLocalOffset(FVector(ProjectileSpeed * DeltaTime, 0.f, 0.f));
}


